import React from 'react';
import './modal.css';

const EmployeeModal = ({ onClose, show, emp }) => {
  const { firstName, lastName, age, dateJoined, jobTitle, avatar, bio } = emp;

  const dateJoinedOn = joined => {
    return new Date(joined).getFullYear();
  };

  const onCloseModal = e => {
    onClose && onClose(e);
  };

  if (!show) {
    return null;
  }

  return (
    <div className="modal-style" id="modal">
      <div className="ui item employee-mod-profile">
        <div className="image employee-mod-image">
          {' '}
          <img src={avatar} alt="profile" />
          <div>Title: {jobTitle}</div>
          <div>Age: {age}</div>
          <div>Joined On: {dateJoinedOn(dateJoined)}</div>
        </div>

        <div className="content">
          <div className="header employee-mod-name">
            {firstName} {lastName}
          </div>
          <div>{bio}</div>
        </div>
      </div>
      <button className="toggle-button" onClick={onCloseModal}>
        <i className="close icon"></i>
      </button>
    </div>
  );
};

export default EmployeeModal;
