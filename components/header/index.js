import React from 'react';

const Header = ({ name, motto, establishDate }) => {
  const estdOn = estDate => {
    return new Date(estDate).getFullYear();
  };

  return (
    <div className="ui block header">
      <h1 className="ui header">{name}</h1>
      <h3 className="ui right floated header">Since {estdOn(establishDate)}</h3>
      <h3 className="ui left floated">{motto}</h3>
    </div>
  );
};

export default Header;
