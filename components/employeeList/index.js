import React, { useState, useEffect } from 'react';
import EmployeeModal from './../employeeModal';
import EmployeeListItem from './../employeeListItem';
import SearchBar from './../searchBar';

import './employeeList.css';

const EmployeeList = ({ list }) => {
  const [searchOn, setSearchOn] = useState(false);
  const [filteredList, setFilteredList] = useState([]);
  const [mainList, setMainlist] = useState([]);
  const [sortBy, setSortBy] = useState('');
  const [selectedEmployee, setSelectedEmployee] = useState();

  useEffect(() => {
    if (!mainList.length) {
      setMainlist(list);
      return;
    }
    if (sortBy === 'firstName') {
      let newList = [...mainList];
      newList.sort((a, b) => {
        if (a.firstName > b.firstName) {
          return 1;
        } else {
          return -1;
        }
      });
      setMainlist(newList);
      return;
    }

    if (sortBy === 'lastName') {
      let newList = [...mainList];
      newList.sort((a, b) => {
        if (a.firstName < b.firstName) {
          return 1;
        } else {
          return -1;
        }
      });
      setMainlist(newList);
      return;
    }

    if (sortBy === 'showAll') {
      setMainlist(list);
    }
  }, [list.length, sortBy]);

  const filterBySearch = name => {
    if (name.length) {
      setSearchOn(true);
      const searchName = name.toLowerCase();
      let temp = mainList.reduce((acc, item) => {
        let tempName = `${item.firstName} ${item.lastName}`;
        if (tempName.toLowerCase().includes(searchName)) {
          acc.push(item);
        }
        return acc;
      }, []);

      setFilteredList(temp);
    } else {
      setSearchOn(false);
      setFilteredList([]);
    }
  };

  const showEmployeeItem = emp => {
    return <EmployeeListItem employee={emp} />;
  };

  return (
    <div>
      <div className="select-search">
        <SearchBar onSearchTermChange={filterBySearch} />
        <div className="select-options">
          <label className="select-label">Sort by</label>
          <select
            value={sortBy}
            onChange={event => setSortBy(event.target.value)}
            className="select-dropdown"
          >
            <option value="showAll">Show All</option>
            <option value="firstName">First Name</option>
            <option value="lastName">Last Name</option>
          </select>
        </div>
      </div>

      <div className="ui divider"></div>

      <div style={{ display: 'flex', flexDirection: 'row', flexWrap: 'wrap' }}>
        {filteredList.length ? (
          filteredList.map(item => {
            return (
              <div onClick={() => setSelectedEmployee(item)} key={item.id}>
                {showEmployeeItem(item)}
              </div>
            );
          })
        ) : mainList && !searchOn ? (
          mainList.map(item => {
            return (
              <div onClick={() => setSelectedEmployee(item)} key={item.id}>
                {showEmployeeItem(item)}
              </div>
            );
          })
        ) : (
          <div className="ui message"> Sorry no results found</div>
        )}
      </div>
      {selectedEmployee && (
        <EmployeeModal
          onClose={() => setSelectedEmployee()}
          show={!!selectedEmployee}
          emp={selectedEmployee}
        />
      )}
    </div>
  );
};

export default EmployeeList;
