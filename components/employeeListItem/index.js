import React, { useState } from 'react';
import './employeeListItem.css';

const EmployeeListItem = ({ employee }) => {
  const { avatar, firstName, lastName, jobTitle } = employee;

  return (
    <div className="ui item employee-profile">
      <div className="image employee-image">
        {' '}
        <img src={avatar} alt="profile" />
      </div>

      <div className="content">
        <div className="header employee-name">
          {' '}
          {firstName} {lastName}{' '}
        </div>
        {jobTitle}
      </div>
    </div>
  );
};

export default EmployeeListItem;
