#### Requirements
*  :white_check_mark: Use React Framework
*  :white_check_mark:  Use Ajax Fetch API to get data
*  :white_check_mark:  Display card Layout with responsiveness 
*  :white_check_mark: Open up a modal on clicking individual cards
*  :white_check_mark: Render the app on mobile devices
*  :white_check_mark: User to Sort by FirstName and Last Name
*  :white_check_mark: User could search based on name search
*  :white_check_mark: Push the changes in gitlab


#### Assumptions / Approach / Improvements / Challenges
1. Have used Flexbox for responsiveness
2. Modal outside click to close the modal - need some time
3. Have used semantic-ui for styling only
4. Take sample-data.json out side of the src dir to fetch the data
5. Used functional components all through out
6. No requirement given to write unit test
7. No additional libraries are used - as suggested in the requirement
8. Scope for further improvement by splitting up into smaller chunks
9. Have done my best given the time constraints
10. Spent a bit more than I thought on sorting - missed immutability concept - not rendering as a result
 
 #### Pushed to Gitlab Pages
[Gitlab pages Link](https://gitlab.com/skumo1/westpac-liberty-suman)