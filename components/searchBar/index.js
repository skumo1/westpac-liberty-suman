import React, { useState } from 'react';
import './searchStyles.css'


const SearchBar = ({ onSearchTermChange }) => {
  const [term, setTerm] = useState('')

  const onInputChange = term => {
    setTerm(term)
    onSearchTermChange(term);
  };

  return (
    <div className="ui search searchBar">
      <div className="ui icon input inputStyle">
        <input
          type="text"
          placeholder="Search name..."
          value={term}
          onChange={event => onInputChange(event.target.value)}
        />
        <i className="search icon"></i>
      </div>
    </div>
  );
};

export default SearchBar;
