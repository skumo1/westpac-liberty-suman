import React, { useEffect, useState } from 'react';
import ReactDOM from 'react-dom';
import './index.css';

import Header from './components/header'
import EmployeeList from './components/employeeList';
import SearchBar from './components/searchBar';

const App = () => {
  const [err, setErr] = useState(false);
  const [info, setInfo] = useState({});
  const [empList, setEmpList] = useState([]);

  useEffect(() => {
    fetch('./sample-data.json')
      .then(res => res.json())
      .then(data => {
        setInfo(data.companyInfo);
        setEmpList(data.employees);
      })
      .catch(err => console.log(err));
  }, []);

  return <div>
    <Header name={info.companyName} motto={info.companyMotto} establishDate={info.companyEst} />
    <EmployeeList list={empList} />
  </div>;
};

ReactDOM.render(<App />, document.getElementById('app'));
